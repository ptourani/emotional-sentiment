require(psych)
require(xtable)

one_without=read.csv("first_half_without.csv",header=F,colClasses=c("numeric","factor","factor","factor","factor","factor","factor"))
colnames(one_without)=c("id","love","joy","surprise","anger","sadness","fear")
levels(one_without$surprise)=levels(one_without$sadness)

two_without=read.csv("second_half_without.csv",header=F,colClasses=c("numeric","factor","factor","factor","factor","factor","factor"))
colnames(two_without)=c("id","love","joy","surprise","anger","sadness","fear")
levels(two_without$surprise)=levels(two_without$sadness)

nr_comments=read.csv("nr_comments.csv",header=F,colClasses=c("numeric","numeric"))
colnames(nr_comments)=c("id","nr")

get_kappas=function(left,right){
    res=rbind(cohen.kappa(cbind(left$love,right$love))$confid[1,],cohen.kappa(cbind(left$joy,right$joy))$confid[1,],cohen.kappa(cbind(left$surprise,right$surprise))$confid[1,],cohen.kappa(cbind(left$anger,right$anger))$confid[1,],cohen.kappa(cbind(left$sadness,right$sadness))$confid[1,],cohen.kappa(cbind(left$fear,right$fear))$confid[1,])
    
    res=cbind(colSums((left==right)[,2:7]),100*colSums((left==right)[,2:7])/nrow(left),colSums((left==right & left=='x')[,2:7]),100*colSums((left==right & left=='x')[,2:7])/nrow(left),colSums((left==right & left=='')[,2:7]),100*colSums((left==right & left=='')[,2:7])/nrow(left),res)

    colnames(res)=c("#common","%agreement","#presence","%agreed presence","#absence","%agreed absence","lower $\\kappa$","Cohen $\\kappa$","upper $\\kappa$")
    rownames(res)=c("love","joy","surprise","anger","sadness","fear")

    res
}

kappas_without=get_kappas(one_without,two_without)
xtable(round(kappas_without, digits=2))

ono=merge(x = one_without, y = nr_comments, by = "id", all.x=TRUE)
ono_one=ono[ono$nr==1,]
ono_ctx=ono[ono$nr>1,]

tno=merge(x = two_without, y = nr_comments, by = "id", all.x=TRUE)
tno_one=tno[tno$nr==1,]
tno_ctx=tno[tno$nr>1,]

xtable(get_kappas(ono_one,tno_one))
xtable(get_kappas(ono_ctx,tno_ctx))

##################


one_with=read.csv("first_half_with.csv",header=F,colClasses=c("numeric","factor","factor","factor","factor","factor","factor"))
colnames(one_with)=c("id","love","joy","surprise","anger","sadness","fear")
levels(one_with$surprise)=levels(one_with$sadness)

two_with=read.csv("second_half_with.csv",header=F,colClasses=c("numeric","factor","factor","factor","factor","factor","factor"))
colnames(two_with)=c("id","love","joy","surprise","anger","sadness","fear")
levels(two_with$surprise)=levels(two_with$sadness)

kappas_with=get_kappas(one_with,two_with)
xtable(round(kappas_with, digits=2))

oyes=merge(x = one_with, y = nr_comments, by = "id", all.x=TRUE)
oyes_one=oyes[oyes$nr==1,]
oyes_ctx=oyes[oyes$nr>1,]

tyes=merge(x = two_with, y = nr_comments, by = "id", all.x=TRUE)
tyes_one=tyes[tyes$nr==1,]
tyes_ctx=tyes[tyes$nr>1,]

xtable(get_kappas(oyes_one,tyes_one))
xtable(get_kappas(oyes_ctx,tyes_ctx))


##################

#how often from 1 to 0, ... for each emotion
change_of_mind=function(oo,to,ow,tw){
    trans=cbind(table(c(oo$love,to$love),c(ow$love,tw$love)),
        table(c(oo$joy,to$joy),c(ow$joy,tw$joy)),
        table(c(oo$surprise,to$surprise),c(ow$surprise,tw$surprise)),
        table(c(oo$anger,to$anger),c(ow$anger,tw$anger)),
        table(c(oo$sadness,to$sadness),c(ow$sadness,tw$sadness)),
        table(c(oo$fear,to$fear),c(ow$fear,tw$fear)))
    colnames(trans)=c(0,1,0,1,0,1,0,1,0,1,0,1)
    rownames(trans)=c(0,1)
    trans
}

xtable(change_of_mind(one_without,two_without,one_with,two_with))
xtable(change_of_mind(ono_one,tno_one,oyes_one,tyes_one))
xtable(change_of_mind(ono_ctx,tno_ctx,oyes_ctx,tyes_ctx))

#for each emotion: more or less agreement?
change_of_agree=function(oo,to,ow,tw){
    agree=cbind(table(c(oo$love==to$love),c(ow$love==tw$love)),
        table(c(oo$joy==to$joy),c(ow$joy==tw$joy)),
        table(c(oo$surprise==to$surprise),c(ow$surprise==tw$surprise)),
        table(c(oo$anger==to$anger),c(ow$anger==tw$anger)),
        table(c(oo$sadness==to$sadness),c(ow$sadness==tw$sadness)),
        table(c(oo$fear==to$fear),c(ow$fear==tw$fear)))
    colnames(agree)=c("d","a","d","a","d","a","d","a","d","a","d","a")
    rownames(agree)=c("d","a")
    agree
}

xtable(change_of_agree(one_without,two_without,one_with,two_with))
xtable(change_of_agree(ono_one,tno_one,oyes_one,tyes_one))
xtable(change_of_agree(ono_ctx,tno_ctx,oyes_ctx,tyes_ctx))



#find ids of comments for love, joy and sadness
sents=(one_without==two_without & one_without=='x')[,c(2,3,6)]
sents2=(one_with==two_with & one_with=='x')[,c(2,3,6)]
intersect(one_without$id[which(sents[,1]==T)],one_with$id[which(sents2[,1]==T)])
intersect(one_without$id[which(sents[,2]==T)],one_with$id[which(sents2[,2]==T)])
intersect(one_without$id[which(sents[,3]==T)],one_with$id[which(sents2[,3]==T)])





