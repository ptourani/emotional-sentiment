require(pgirmess)
require(xtable)

love=read.csv("out_love.csv",header=T)
joy=read.csv("out_joy.csv",header=T)
sadness=read.csv("out_sadness.csv",header=T)

nr_comments=read.csv("nr_comments.txt",header=F)
colnames(nr_comments)=c("report_id","issue_id","nr_comments")

love=merge(x = love, y = nr_comments, by = "issue_id", all.x=TRUE)
joy=merge(x = joy, y = nr_comments, by = "issue_id", all.x=TRUE)
sadness=merge(x = sadness, y = nr_comments, by = "issue_id", all.x=TRUE)

love=data.frame(love,emotion=factor(rep("love",times=nrow(love)),levels=c("love","joy","sadness")))
joy=data.frame(joy,emotion=factor(rep("joy",times=nrow(joy)),levels=c("love","joy","sadness")))
sadness=data.frame(sadness,emotion=factor(rep("sadness",times=nrow(sadness)),levels=c("love","joy","sadness")))

all=rbind(love,joy,sadness)

#fixing time
boxplot(fixing_time ~ emotion, data=all)
kruskal.test(fixing_time ~ emotion, data=all)
kruskalmc(fixing_time ~ emotion, data=all)

summary(love$fixing_time)
summary(joy$fixing_time)
summary(sadness$fixing_time)


#nr of comments
boxplot(nr_comments ~ emotion, data=all)
kruskal.test(nr_comments ~ emotion, data=all)
kruskalmc(nr_comments ~ emotion, data=all)

summary(love$nr_comments)
summary(joy$nr_comments)
summary(sadness$nr_comments)


#nr of watchers
boxplot(issue_watchers ~ emotion, data=all)
kruskal.test(issue_watchers ~ emotion, data=all)
kruskalmc(issue_watchers ~ emotion, data=all)

summary(love$issue_watchers)
summary(joy$issue_watchers)
summary(sadness$issue_watchers)


#issue_type vs. emotion
tt=table(all$issue_type,all$emotion)
100*tt/colSums(tt)
chisq.test(all$issue_type,all$emotion)


#xtable(round(kappas, digits=2))
