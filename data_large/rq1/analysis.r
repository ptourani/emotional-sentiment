require(psych)
require(xtable)

analyze=function(half1,half2){
one=read.csv(half1,header=F)
colnames(one)=c("id","love","joy","surprise","anger","sadness","fear")

two=read.csv(half2,header=F)
colnames(two)=c("id","love","joy","surprise","anger","sadness","fear")

kappas=rbind(cohen.kappa(cbind(one$love,two$love))$confid[1,],cohen.kappa(cbind(one$joy,two$joy))$confid[1,],cohen.kappa(cbind(one$surprise,two$surprise))$confid[1,],cohen.kappa(cbind(one$anger,two$anger))$confid[1,],cohen.kappa(cbind(one$sadness,two$sadness))$confid[1,],cohen.kappa(cbind(one$fear,two$fear))$confid[1,])
kappas=cbind(colSums((one==two)[,2:7]),100*colSums((one==two)[,2:7])/nrow(one),100*colSums((one==two & one=='x')[,2:7])/nrow(one),100*colSums((one==two & one=='')[,2:7])/nrow(one),kappas)

colnames(kappas)=c("#common","%agreement","%agreed presence","%agreed absence","lower kappa","Cohen kappa","upper kappa")
rownames(kappas)=c("love","joy","surprise","anger","sadness","fear")
print(xtable(round(kappas, digits=2)))
}

#each group has 2 raters for each comment, so grp1_first_half is 1st rater for each comment, grp1_second_half is 2nd rater
analyze("grp_1_round_1_first_half.txt","grp_1_round_1_second_half.txt")
analyze("grp_2_round_1_first_half.txt","grp_2_round_1_second_half.txt")
analyze("grp_1_round_2_first_half.txt","grp_1_round_2_second_half.txt")
analyze("grp_2_round_2_first_half.txt","grp_2_round_2_second_half.txt")

#####


#find ids of comments for love, joy and sadness
sents=(one==two & one=='x')[,c(2,3,6)]
one$id[which(sents[,1]==T)]
one$id[which(sents[,2]==T)]
one$id[which(sents[,3]==T)]
