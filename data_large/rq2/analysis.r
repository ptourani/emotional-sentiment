require(irr)
require(xtable)

myfleiss=function(ratings, alpha = 0.05, exact = FALSE, detail = FALSE) 
{
    ratings <- as.matrix(na.omit(ratings))
    ns <- nrow(ratings)
    nr <- ncol(ratings)
    lev <- levels(as.factor(ratings))
    for (i in 1:ns) {
        frow <- factor(ratings[i, ], levels = lev)
        if (i == 1) 
            ttab <- as.numeric(table(frow))
        else ttab <- rbind(ttab, as.numeric(table(frow)))
    }
    ttab <- matrix(ttab, nrow = ns)
    agreeP <- sum((apply(ttab^2, 1, sum) - nr)/(nr * (nr - 1))/ns)
    if (!exact) {
        method <- "Fleiss' Kappa for m Raters"
        chanceP <- sum(apply(ttab, 2, sum)^2)/(ns * nr)^2
    }
    else {
        method <- "Fleiss' Kappa for m Raters (exact value)"
        for (i in 1:nr) {
            rcol <- factor(ratings[, i], levels = lev)
            if (i == 1) 
                rtab <- as.numeric(table(rcol))
            else rtab <- rbind(rtab, as.numeric(table(rcol)))
        }
        rtab <- rtab/ns
        chanceP <- sum(apply(ttab, 2, sum)^2)/(ns * nr)^2 - sum(apply(rtab, 
            2, var) * (nr - 1)/nr)/(nr - 1)
    }
    value <- (agreeP - chanceP)/(1 - chanceP)
    if (!exact) {
        pj <- apply(ttab, 2, sum)/(ns * nr)
        qj <- 1 - pj
        varkappa <- (2/(sum(pj * qj)^2 * (ns * nr * (nr - 1)))) * 
            (sum(pj * qj)^2 - sum(pj * qj * (qj - pj)))
        SEkappa <- sqrt(varkappa)
        u <- value/SEkappa
        p.value <- 2 * (1 - pnorm(abs(u)))
        ci=value+(c(-1,1)*(abs(pnorm(alpha/2))*SEkappa)) #see http://www.mathworks.com/matlabcentral/fileexchange/15426-fleisses-kappa/content/fleiss.m (u == z, check out pnorm!)
        if (detail) {
            pj <- apply(ttab, 2, sum)/(ns * nr)
            pjk <- (apply(ttab^2, 2, sum) - ns * nr * pj)/(ns * 
                nr * (nr - 1) * pj)
            kappaK <- (pjk - pj)/(1 - pj)
            varkappaK <- 2/(ns * nr * (nr - 1))
            SEkappaK <- sqrt(varkappaK)
            uK <- kappaK/SEkappaK
            p.valueK <- 2 * (1 - pnorm(abs(uK)))
            tableK <- as.table(round(cbind(kappaK, uK, p.valueK), 
                digits = 3))
            rownames(tableK) <- lev
            colnames(tableK) <- c("Kappa", "z", "p.value")
        }
    }
    if (!exact) {
        if (!detail) {
            rval <- list(method = method, subjects = ns, raters = nr, 
                irr.name = "Kappa", value = value)
        }
        else {
            rval <- list(method = method, subjects = ns, raters = nr, 
                irr.name = "Kappa", value = value, detail = tableK)
        }
        rval <- c(rval, stat.name = "z", statistic = u, p.value = p.value, ci = ci)
    }
    else {
        rval <- list(method = method, subjects = ns, raters = nr, 
            irr.name = "Kappa", value = value)
    }
    class(rval) <- "irrlist"
    return(rval)
}

#conflicts=read.csv("conflicts.csv",header=T)

process=function(index,data){
    #select only the (comment,emotion)-pairs that we considered
    res=c()
    data1=data[,index]
    res=cbind(res,as.vector(t(as.matrix(data1))))
    data2=data[,6+index]
    res=cbind(res,as.vector(t(as.matrix(data2))))
    data3=data[,12+index]
    res=cbind(res,as.vector(t(as.matrix(data3))))
    data4=data[,18+index]
    res=cbind(res,as.vector(t(as.matrix(data4))))
    res=as.matrix(res)
    res[res=="x"]=1
    res[res==""]=0
    class(res)="numeric"
    res
}

all_emotions=function(data){
#all emotions
#res=process(2:7,conflicts[,2:7])
#sres=rowSums(res)
#c(nrow(res),(sum(sres==4)+sum(4-sres==4))/nrow(res)*100,sum(sres==4),(sum(sres>=3)+sum(4-sres>=3))/nrow(res)*100,sum(sres>=3),unlist(myfleiss(res)[c("ci1","value","ci2")]))

#without joy and anger
#res=process(c(1,3,5,6),conflicts[,c(1,3,5,6)])
#sres=rowSums(res)
#c(nrow(res),(sum(sres==4)+sum(4-sres==4))/nrow(res)*100,sum(sres==4),(sum(sres>=3)+sum(4-sres>=3))/nrow(res)*100,sum(sres>=3),unlist(myfleiss(res)[c("ci1","value","ci2")]))

#each emotion separately
tab=c()
res=process(2,data)
sres=rowSums(res)
#rowMeans(cbind(as.vector(unlist(myfleiss(res[,c(1,2,3)])[c("ci1","value","ci2")])),as.vector(unlist(myfleiss(res[,c(1,2,4)])[c("ci1","value","ci2")])),as.vector(unlist(myfleiss(res[,c(1,3,4)])[c("ci1","value","ci2")])),as.vector(unlist(myfleiss(res[,c(2,3,4)])[c("ci1","value","ci2")]))))
tab=rbind(tab,c(nrow(res),(sum(sres==4)+sum(4-sres==4))/nrow(res)*100,sum(sres==4),(sum(sres>=3)+sum(4-sres>=3))/nrow(res)*100,sum(sres>=3),unlist(myfleiss(res)[c("ci1","value","ci2")])))
three_or_more=((sres>=3))
print(data[three_or_more,1])
res=process(3,data)
sres=rowSums(res)
tab=rbind(tab,c(nrow(res),(sum(sres==4)+sum(4-sres==4))/nrow(res)*100,sum(sres==4),(sum(sres>=3)+sum(4-sres>=3))/nrow(res)*100,sum(sres>=3),unlist(myfleiss(res)[c("ci1","value","ci2")])))
three_or_more=((sres>=3))
print(data[three_or_more,1])
res=process(4,data)
sres=rowSums(res)
tab=rbind(tab,c(nrow(res),(sum(sres==4)+sum(4-sres==4))/nrow(res)*100,sum(sres==4),(sum(sres>=3)+sum(4-sres>=3))/nrow(res)*100,sum(sres>=3),unlist(myfleiss(res)[c("ci1","value","ci2")])))
three_or_more=((sres>=3))
print(data[three_or_more,1])
res=process(5,data)
sres=rowSums(res)
tab=rbind(tab,c(nrow(res),(sum(sres==4)+sum(4-sres==4))/nrow(res)*100,sum(sres==4),(sum(sres>=3)+sum(4-sres>=3))/nrow(res)*100,sum(sres>=3),unlist(myfleiss(res)[c("ci1","value","ci2")])))
three_or_more=((sres>=3))
print(data[three_or_more,1])
res=process(6,data)
sres=rowSums(res)
tab=rbind(tab,c(nrow(res),(sum(sres==4)+sum(4-sres==4))/nrow(res)*100,sum(sres==4),(sum(sres>=3)+sum(4-sres>=3))/nrow(res)*100,sum(sres>=3),unlist(myfleiss(res)[c("ci1","value","ci2")])))
three_or_more=((sres>=3))
print(data[three_or_more,1])
res=process(7,data)
sres=rowSums(res)
tab=rbind(tab,c(nrow(res),(sum(sres==4)+sum(4-sres==4))/nrow(res)*100,sum(sres==4),(sum(sres>=3)+sum(4-sres>=3))/nrow(res)*100,sum(sres>=3),unlist(myfleiss(res)[c("ci1","value","ci2")])))
three_or_more=((sres>=3))
print(data[three_or_more,1])

colnames(tab)=c("total #","%4","#4x","%>=3","#>=3x","lower $\\kappa$","Fleiss $\\kappa$","upper $\\kappa$")
rownames(tab)=c("love","joy","surprise","anger","sadness","fear")
print(xtable(round(tab, digits=2)))
}


#round 1 data
data_round_1=read.csv("grp_1_grp_2_round_1.txt",header=F)
all_emotions(data_round_1)

#round 2 data
data_round_2=read.csv("grp_1_grp_2_round_2.txt",header=F)
all_emotions(data_round_2)

#aggregated round data
data_round=read.csv("grp_1_grp_2_round.txt",header=F)
all_emotions(data_round)

#without context data
data_without=read.csv("grp_1_grp_2_without.txt",header=F)
all_emotions(data_without)

#with context data
data_with=read.csv("grp_1_grp_2_with.txt",header=F)
all_emotions(data_with)

#####


#find ids of comments for love, joy and sadness
index=2
res=process(index,data)
sres=rowSums(res)
#CHANGE LINE BELOW
#data$id[which(conflicts[,index]=="x")[sres>=3]]

index=3
res=process(index,data)
sres=rowSums(res)
#CHANGE LINE BELOW
#data$id[which(conflicts[,index]=="x")[sres>=3]]

index=6
res=process(index,data)
sres=rowSums(res)
#CHANGE LINE BELOW
#data$id[which(conflicts[,index]=="x")[sres>=3]]





##this is incorrect: treated each comment as fully evaluated (all emotions), whereas we only looked at some emotions of each comment
#tab=c()
#aa=apply(cbind(data$love1,data$love2,data$love3,data$love4), 1, function(x) sum(x==2))
#ab=rbind(tab,c((sum(aa==4)+sum(4-aa==4))/length(aa)*100,sum(aa==4),(sum(aa==3)+sum(4-aa==3))/length(aa)*100,sum(aa==3),(sum(aa==2))/length(aa)*100,sum(aa==2),unlist(myfleiss(cbind(data$love1,data$love2,data$love3,data$love4))[c("ci1","value","ci2")])))
#aa=apply(cbind(data$joy1,data$joy2,data$joy3,data$joy4), 1, function(x) sum(x==2))
#tab=rbind(tab,c((sum(aa==4)+sum(4-aa==4))/length(aa)*100,sum(aa==4),(sum(aa==3)+sum(4-aa==3))/length(aa)*100,sum(aa==3),(sum(aa==2))/length(aa)*100,sum(aa==2),unlist(myfleiss(cbind(data$joy1,data$joy2,data$joy3,data$joy4))[c("ci1","value","ci2")])))
#aa=apply(cbind(data$surprise1,data$surprise2,data$surprise3,data$surprise4), 1, function(x) sum(x==2))
#tab=rbind(tab,c((sum(aa==4)+sum(4-aa==4))/length(aa)*100,sum(aa==4),(sum(aa==3)+sum(4-aa==3))/length(aa)*100,sum(aa==3),(sum(aa==2))/length(aa)*100,sum(aa==2),unlist(myfleiss(cbind(data$surprise1,data$surprise2,data$surprise3,data$surprise4))[c("ci1","value","ci2")])))
#aa=apply(cbind(data$anger1,data$anger2,data$anger3,data$anger4), 1, function(x) sum(x==2))
#tab=rbind(tab,c((sum(aa==4)+sum(4-aa==4))/length(aa)*100,sum(aa==4),(sum(aa==3)+sum(4-aa==3))/length(aa)*100,sum(aa==3),(sum(aa==2))/length(aa)*100,sum(aa==2),unlist(myfleiss(cbind(data$anger1,data$anger2,data$anger3,data$anger4))[c("ci1","value","ci2")])))
#aa=apply(cbind(data$sadness1,data$sadness2,data$sadness3,data$sadness4), 1, function(x) sum(x==2))
#tab=rbind(tab,c((sum(aa==4)+sum(4-aa==4))/length(aa)*100,sum(aa==4),(sum(aa==3)+sum(4-aa==3))/length(aa)*100,sum(aa==3),(sum(aa==2))/length(aa)*100,sum(aa==2),unlist(myfleiss(cbind(data$sadness1,data$sadness2,data$sadness3,data$sadness4))[c("ci1","value","ci2")])))
#aa=apply(cbind(data$fear1,data$fear2,data$fear3,data$fear4), 1, function(x) sum(x==2))
#tab=rbind(tab,c((sum(aa==4)+sum(4-aa==4))/length(aa)*100,sum(aa==4),(sum(aa==3)+sum(4-aa==3))/length(aa)*100,sum(aa==3),(sum(aa==2))/length(aa)*100,sum(aa==2),unlist(myfleiss(cbind(data$fear1,data$fear2,data$fear3,data$fear4))[c("ci1","value","ci2")])))
#colnames(tab)=c("%4","#4x","%3","#3x","%2","#2x","lower $\\kappa$","Fleiss $\\kappa$","upper $\\kappa$")
#rownames(tab)=c("love","joy","surprise","anger","sadness","fear")
#xtable(round(tab, digits=2))
